package com.devops.suma.services;

import com.devops.suma.data.SumaEntity;

public interface SumaServices {
	public SumaEntity sumar(double op1, double op2);
}
