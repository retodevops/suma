package com.devops.suma.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devops.suma.business.SumaBusiness;
import com.devops.suma.data.SumaEntity;
@Service
public class SumaServicesImpl implements SumaServices {

	@Autowired
	private SumaBusiness business;
	
	@Override
	public SumaEntity sumar(double op1, double op2) {
		String clave = String.valueOf((int)op1) +"-"+ String.valueOf((int)op2);
		return business.sumar(clave, op1, op2);
	}

}
