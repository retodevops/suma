package com.devops.suma.business;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.devops.suma.data.SumaEntity;
import com.devops.suma.data.SumaRepository;

@Component
public class SumaBusiness {
	@Autowired
	private SumaRepository repository;
	
	public SumaEntity sumar(String clave, double op1, double op2)
	{
		Optional<SumaEntity> suma = repository.findById(clave);
		SumaEntity se = null;
		if(!suma.isPresent())
		{
			se= new SumaEntity(clave, op1, op2);
			repository.save(se);
		}
		else
		{
			se = suma.get();
		}
		return se;
	}
}
